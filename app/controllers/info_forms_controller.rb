class InfoFormsController < ApplicationController
  def new
    @info_form = InfoForm.new
  end
  
  def create
    @info_form = InfoForm.new(info_form_params)
    respond_to do |format|
      if @info_form.save
        format.html { redirect_to '/', notice: 'Contact info sent' }
      else
        format.html { redirect_to '/', notice: 'Error sending contact info, try again' }
      end
    end
  end

  private

  def info_form_params
    params.require(:info_form).permit(:first_name, :last_name, :email)
  end
end
