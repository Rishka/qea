class EventsController < ApplicationController
  def show
    @info_form = InfoForm.new
    @event = Event.find params[:id]
  end

  def index
    @events = Event.rank(:row_order).all
  end

  def rank
    params[:rank].each do |event|
      Event.find(event[1]['id'].to_i).update_attribute :row_order_position, event[1]['position'].to_i
    end
    render nothing: true
  end
end
