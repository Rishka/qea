class Event < ActiveRecord::Base
  include RankedModel
  has_many :packages
  ranks :row_order
  after_save :position
  def days_until_start
    return 'The event has passed' if DateTime.now > starts_on
    (starts_on.to_date - Date.today).to_i
  end

  def position
    return unless row_order.nil?
    update_attribute :row_order_position, :last
  end
end

