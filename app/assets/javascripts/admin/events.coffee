# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
set_position =->
  $('.sort-row').each (i)->
    $(this).attr('data-pos', i+1)

ready =->
  AUTH_TOKEN = $('meta[name=csrf-token]').content
  set_position()
  $('.sortable').sortable()
  $('.sortable').sortable().bind 'sortupdate', (e, ui) ->
    updated_rank_position = []
    set_position()
    $('.sort-row').each (i)->
      updated_rank_position.push
        id: $(this).data('id')
        position: i + 1
    $.ajax
      type: 'PUT'
      url: "/events/rank"
      data: rank: updated_rank_position
    return

$(document).ready ready
$(document).on 'page:load', ready()
