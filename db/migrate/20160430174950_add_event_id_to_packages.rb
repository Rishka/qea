class AddEventIdToPackages < ActiveRecord::Migration
  def change
    change_table :packages do |t|
      t.belongs_to :event, index: true
    end
  end
end
