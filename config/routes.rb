Rails.application.routes.draw do
  root 'events#index'

  resources :events, only: [:show, :index]
  resources :events do
    put 'rank', on: :collection
  end
  resources :info_forms, only: :create
  namespace :admin do
    resources :events, :packages
  end
end
